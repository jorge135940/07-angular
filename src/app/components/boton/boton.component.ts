import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boton',
  templateUrl: './boton.component.html',
  styleUrls: ['./boton.component.css']
})

export class BotonComponent implements OnInit {

  opcion: number=0; 
  
  func1ofB1() {console.log("Aun no se presionó el boton 1");}
  func2ofB1() {
    console.log("Se presionó el boton 1")
    this.opcion=1;
  }
  
  func1ofB2() {console.log("Aun no se presionó el boton 2");}
  func2ofB2() {
    console.log("Se presionó el boton 2")
    this.opcion=2;
  }
  
  func1ofB3() {console.log("Aun no se presionó el boton 3");}
  func2ofB3() {
    console.log("Se presionó el boton 3")
    this.opcion=3;
  }

  

  constructor() { }

  ngOnInit(): void {
  }

}




  


  
  
